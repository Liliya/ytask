package com.example.lilia_markova.ytask.util;

import com.example.lilia_markova.ytask.model.Good;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

/**
 * Created by Lilia_Markova on 6/17/2015.
 */
public class CustomSerializer implements JsonSerializer<Good> {
    public JsonElement serialize(Good src,
                                 Type typeOfSrc,
                                 JsonSerializationContext context) {
        Gson gson = new Gson();
        JsonObject good = new JsonObject();
        good.add("name", gson.toJsonTree(src.getName()));
        good.add("count", gson.toJsonTree(src.getCount()));

        return good;
    }
}

