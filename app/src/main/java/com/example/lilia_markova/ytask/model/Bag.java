package com.example.lilia_markova.ytask.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lilia_Markova on 6/17/2015.
 */
public class Bag {

    private List<Good> goods = new ArrayList<Good>();
    private BigInteger sum = BigInteger.ZERO;

    public Bag(List<Good> goods) {
        this.goods = goods;
        calculateSum();
    }

    public void add(List<Good> newGoods) {
        for (Good newGood : newGoods) {
            add(newGood);
        }
    }

    public BigInteger getSum() {
        return sum;
    }

    public void calculateSum() {
        sum = BigInteger.ZERO;
        for (Good good : goods) {
            sum = sum.add(good.getTotal());
        }
    }

    public void add(Good newGood) {
        for (Good good : goods) {
            if (good.getId() == newGood.getId()) {
                good.setCount(good.getCount() + newGood.getCount());
            }
        }
        calculateSum();
    }

    public List<Good> getGoods() {
        return goods;
    }

    public void add(Bag bag) {
        add(bag.getGoods());
    }

    public void setCountToNull() {
        for (Good good : goods) {
            good.setCount(0);
        }
        calculateSum();
    }

    @Override
    public String toString() {
        return "Bag{" +
                "goods=" + goods +
                ", sum=" + sum +
                '}';
    }
}
