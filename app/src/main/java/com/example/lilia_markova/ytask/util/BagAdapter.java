package com.example.lilia_markova.ytask.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.lilia_markova.ytask.R;
import com.example.lilia_markova.ytask.activity.BagActivity;
import com.example.lilia_markova.ytask.model.Good;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Lilia_Markova on 6/16/2015.
 */
public class BagAdapter extends BaseAdapter {
    private LayoutInflater lInflater;
    List<Good> goods = new LinkedList<Good>();
    BagActivity activity;

    public BagAdapter(BagActivity actv, List<Good> goods) {
        lInflater = (LayoutInflater) actv.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        activity = actv;
        for(Good good : goods){
            if(good.getCount()!=0){
                this.goods.add(good);
            }
        }
    }

    @Override
    public int getCount() {
        return goods.size();
    }

    @Override
    public Object getItem(int i) {
        return goods.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = lInflater.inflate(R.layout.item_bag, viewGroup, false);
        }
        final Good good = (Good) goods.get(i);
        TextView nameTextView = (TextView) view.findViewById(R.id.nameTextView);
        nameTextView.setText(good.getName());

        TextView costTextView = (TextView) view.findViewById(R.id.costTextView);
        costTextView.setText( good.getCost() + "$");

        final TextView totalTextView = (TextView) view.findViewById(R.id.totalTextView);
        totalTextView.setText( good.getTotalString() );

        final TextView countTextView = (TextView) view.findViewById(R.id.countTextView);
        countTextView.setText( String.valueOf( good.getCount()));


        Button buttonDelete = (Button) view.findViewById( R.id.buttonDelete);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.delete(good);
            }
        });



        return view;
    }
}
