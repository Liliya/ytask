package com.example.lilia_markova.ytask.activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.lilia_markova.ytask.R;
import com.example.lilia_markova.ytask.model.Good;
import com.example.lilia_markova.ytask.util.BagAdapter;
import com.example.lilia_markova.ytask.util.DataManager;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class BagActivity extends Activity {

    ListView listView;
    List<Good> goods;
    MenuItem bagView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bag);
        listView = (ListView) findViewById( R.id.listView);
        Button buttonSend = (Button) findViewById( R.id.buttonSend );
        getActionBar().setDisplayHomeAsUpEnabled(true);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail();
                deleteAll();

            }
        });
        goods = DataManager.getBag(this);
        if(goods != null){
            listView.setAdapter(new BagAdapter(this, goods));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bag, menu);
        bagView = menu.findItem(R.id.action_bag);
        updateBag();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    public void delete(Good good){
        good.setCount(0);
        DataManager.saveBag(this, goods);
        listView.setAdapter(new BagAdapter(this, goods));
        updateBag();
    }

    public void updateBag(){
        BigInteger sum = new BigInteger("0");
        for( Good good : goods){
            sum = sum.add(good.getTotal());
        }
        bagView.setTitle(sum + "$");
    }

    public void deleteAll(){
        goods = new ArrayList<Good>();
        listView.setAdapter( new BagAdapter(this, goods));
        DataManager.saveBag(this, goods);
        updateBag();
    }

    public void sendEmail(){
        if(goods.size() == 0){
            Toast.makeText(this, getResources().getString(R.string.bag_is_empty), Toast.LENGTH_SHORT).show();
        } else {
            File file = DataManager.getBagFileLocation(this);
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/html");
            intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString( R.string.order) );
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            TelephonyManager tMgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            String mPhoneNumber = tMgr.getLine1Number();

            AccountManager accountManager = AccountManager.get(this);
            Account[] accounts = accountManager.getAccountsByType("com.google");
            Account account;
            String email;
            if (accounts.length > 0) {
                account = accounts[0];
                email = account.name;
            } else {
                email = "";
            }
            intent.putExtra(Intent.EXTRA_TEXT, getResources().getString( R.string.number) + mPhoneNumber +  getResources().getString( R.string.email) + email);

            startActivity(Intent.createChooser(intent, "Send Email"));
        }
    }

}
