package com.example.lilia_markova.ytask.model;

import java.math.BigInteger;

/**
 * Created by Lilia_Markova on 6/16/2015.
 */
public class Good {

    protected String name;
    protected int count;
    private int id;
    private BigInteger cost;
    private BigInteger total;

    public Good(Good good) {
        this.name = good.name;
        this.count = good.count;
        this.id = good.id;
        this.cost = good.cost;
        this.total = good.total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigInteger getCost() {
        return cost;
    }


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
        total = cost.multiply(new BigInteger(String.valueOf(count)));
    }

    public BigInteger getTotal() {
        if (total == null) {
            total = BigInteger.ZERO;
        }
        return total;
    }

    public String getTotalString() {
        return count + " x " + cost + "$ = " + total + "$ total";
    }

    @Override
    public String toString() {
        return "Good{" +
                "name='" + name + '\'' +
                ", count=" + count +
                ", id=" + id +
                ", cost=" + cost +
                ", total=" + total +
                '}';
    }
}
