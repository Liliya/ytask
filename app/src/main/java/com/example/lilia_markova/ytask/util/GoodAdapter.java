package com.example.lilia_markova.ytask.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.lilia_markova.ytask.R;
import com.example.lilia_markova.ytask.activity.MainActivity;
import com.example.lilia_markova.ytask.model.Good;

import java.util.List;

/**
 * Created by Lilia_Markova on 6/16/2015.
 */
public class GoodAdapter extends BaseAdapter {
    private LayoutInflater lInflater;
    List<Good> goods;
    MainActivity activity;

    public GoodAdapter(MainActivity actv, List<Good> goods) {
        lInflater = (LayoutInflater) actv.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        activity = actv;
        this.goods = goods;
    }

    @Override
    public int getCount() {
        return goods.size();
    }

    @Override
    public Object getItem(int i) {
        return goods.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = lInflater.inflate(R.layout.item_good, viewGroup, false);
        }
        final Good good = (Good) goods.get(i);
        TextView nameTextView = (TextView) view.findViewById(R.id.nameTextView);
        nameTextView.setText(good.getName());

        TextView costTextView = (TextView) view.findViewById(R.id.costTextView);
        costTextView.setText( good.getCost() + "$" );

        final TextView totalTextView = (TextView) view.findViewById(R.id.totalTextView);
        totalTextView.setText( good.getTotalString()  );

        SeekBar seekBar = (SeekBar) view.findViewById( R.id.seekBar);
        seekBar.setMax(10);
        seekBar.setProgress( good.getCount() );
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                good.setCount(progress);
                totalTextView.setText( good.getTotalString() );
                activity.updateCurrentBagSum();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return view;
    }
}
