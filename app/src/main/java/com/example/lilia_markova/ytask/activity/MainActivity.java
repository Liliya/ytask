package com.example.lilia_markova.ytask.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.lilia_markova.ytask.R;
import com.example.lilia_markova.ytask.model.Bag;
import com.example.lilia_markova.ytask.model.Good;
import com.example.lilia_markova.ytask.service.DownloadReceiver;
import com.example.lilia_markova.ytask.service.DownloadService;
import com.example.lilia_markova.ytask.util.DataManager;
import com.example.lilia_markova.ytask.util.GoodAdapter;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity {

    ProgressDialog progressDialog;
    ListView listView;
    TextView currentBagView;
    Button buttonToBag;
    Context ctx;

    MenuItem totalBagView;

    Bag totalBag;
    Bag currentBag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ctx = this;

        listView = (ListView) findViewById(R.id.listView);
        currentBagView = (TextView) findViewById(R.id.totalTextView);
        buttonToBag = (Button) findViewById(R.id.buttonToBag);

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getResources().getString(R.string.wait));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMax(100);
        progressDialog.show();

        Intent intent = new Intent(this, DownloadService.class);
        intent.putExtra(DataManager.LENGTH, DataManager.getFileLength(this));

        DownloadReceiver receiver = new DownloadReceiver(new Handler());
        receiver.setProgressDialog(progressDialog);
        receiver.setActivity(this);
        intent.putExtra("receiver", receiver);
        startService(intent);

        buttonToBag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentBag != null) {
                    addCurrentBagToTotal();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bag, menu);
        totalBagView = menu.findItem(R.id.action_bag);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        totalBag = new Bag (DataManager.getBag(this));
        updateTotalBagView();
    }

    public void initializeBags( List<Good> goods ){
        List<Good> goods2 = new ArrayList<Good>();

        for(Good good : goods){
            goods2.add(new Good(good));
        }
        totalBag = new Bag(  goods2);
        currentBag = new Bag( goods );
        updateList();
        updateTotalBagView();
        updateCurrentBagView();
    }

    public void addCurrentBagToTotal(){
        totalBag.add(currentBag);
        currentBag.setCountToNull();
        updateList();
        updateTotalBagView();
        updateCurrentBagView();
    }



    public void updateTotalBagView(){
        if(totalBagView != null) {
            totalBagView.setTitle(totalBag.getSum() + "$");
        }
    }

    public void updateCurrentBagView(){
        currentBagView.setText( currentBag.getSum() + "$");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_bag:
                saveCurrent();
                startActivity(new Intent(ctx, BagActivity.class));
                break;
        }
        return true;
    }


    public void updateList() {
        listView.setAdapter(new GoodAdapter(this, currentBag.getGoods()));
        updateCurrentBagView();
    }

    public void updateCurrentBagSum( ){
        currentBag.calculateSum();
        updateCurrentBagView();
        Log.d("app", "cur bag = " + currentBag.toString());
        Log.d("app", "total bag = " + totalBag.toString());
    }


    public void saveCurrent() {
        DataManager.saveBag(this, totalBag.getGoods());
    }

    @Override
    protected void onDestroy() {
        DataManager.saveBag(this, new ArrayList<Good>());
        super.onDestroy();
    }
}
