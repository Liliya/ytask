package com.example.lilia_markova.ytask.service;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.widget.Toast;

import com.example.lilia_markova.ytask.activity.MainActivity;
import com.example.lilia_markova.ytask.util.DataManager;

/**
 * Created by Lilia_Markova on 6/16/2015.
 */
public class DownloadReceiver extends ResultReceiver {
    ProgressDialog mProgressDialog;
    MainActivity activity;

    public DownloadReceiver(Handler handler) {
        super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        super.onReceiveResult(resultCode, resultData);
        if (resultCode == DownloadService.UPDATE_PROGRESS) {
            int progress = resultData.getInt(DownloadService.PROGRESS);
            if(mProgressDialog != null) {
                mProgressDialog.setProgress(progress);
                if (progress == 100) {
                    mProgressDialog.dismiss();
                    activity.initializeBags(DataManager.getGoodsFromFile());
                }
            }
        } else if(resultCode == DownloadService.FILE_LENGTH){
            DataManager.saveFileLength(activity, resultData.getInt(DownloadService.LENGTH));
        }
        if(resultCode == DownloadService.ERROR){
            int error = resultData.getInt(DownloadService.PROGRESS);
            if(error == 1){
                Toast.makeText(activity, "Connection error", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(activity, "Error!", Toast.LENGTH_SHORT).show();
            }
            mProgressDialog.dismiss();
        }
    }

    public void setProgressDialog(ProgressDialog pd){
        mProgressDialog = pd;
    }
    public void setActivity(MainActivity activity){
        this.activity = activity;
    }
}
