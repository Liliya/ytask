package com.example.lilia_markova.ytask.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;
import android.util.Log;

import com.example.lilia_markova.ytask.util.DataManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Created by Lilia_Markova on 6/16/2015.
 */
public class DownloadService extends IntentService {

    public static final int UPDATE_PROGRESS = 111;
    public static final int FILE_LENGTH = 222;
    public static final int ERROR = 404;

    public static final String PROGRESS = "progress";
    public static final String LENGTH = "length";
    public static final String RECEIVER = "receiver";
    public static final String ERROR_CODE = "error";

    private String urlToDownload = "https://www.dropbox.com/s/aq29cm5hlz8s9gw/yota.txt?dl=1";

    public DownloadService() {
        super("DownloadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ResultReceiver receiver = (ResultReceiver) intent.getParcelableExtra( RECEIVER );
        try {
            int oldLength = intent.getIntExtra(DataManager.LENGTH, 0);
            URL url = new URL(urlToDownload);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            int fileLength = connection.getContentLength();
            if (oldLength != fileLength) {
                Bundle resultData2 = new Bundle();
                resultData2.putInt(LENGTH, fileLength);
                receiver.send(FILE_LENGTH, resultData2);
                InputStream in = connection.getInputStream();
                File testDirectory = new File(
                        Environment.getExternalStorageDirectory() + "/Download");
                if (!testDirectory.exists()) {
                    testDirectory.mkdir();
                }
                FileOutputStream fos = new FileOutputStream(testDirectory
                        + "/goods.txt");
                byte data[] = new byte[1024];
                long total = 0;
                int count;
                while ((count = in.read(data)) != -1) {
                    total += count;
                    Bundle resultData = new Bundle();
                    resultData.putInt(PROGRESS, (int) (total * 100 / fileLength));
                    receiver.send(UPDATE_PROGRESS, resultData);
                    fos.write(data, 0, count);

                }
                Log.d("app", "downloading");
                fos.flush();
                fos.close();
                in.close();


            } else {
                Log.d("app", "without downloading");
            }
            Bundle resultData = new Bundle();
            resultData.putInt(PROGRESS, 100);
            receiver.send(UPDATE_PROGRESS, resultData);
        } catch( UnknownHostException e1){
            e1.printStackTrace();
            Bundle resultData = new Bundle();
            resultData.putInt(ERROR_CODE, 1);
            receiver.send(ERROR, resultData);

        } catch( MalformedURLException e1){
            e1.printStackTrace();
            Bundle resultData = new Bundle();
            resultData.putInt(ERROR_CODE, 1);
            receiver.send(ERROR, resultData);

        }catch (IOException e) {
            Bundle resultData = new Bundle();
            resultData.putInt(ERROR_CODE, 2);
            receiver.send(ERROR, resultData);
            e.printStackTrace();
        } finally {

        }
    }
}
