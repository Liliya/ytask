package com.example.lilia_markova.ytask.util;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Environment;

import com.example.lilia_markova.ytask.model.Good;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Lilia_Markova on 6/16/2015.
 */
public class DataManager {

    private static String BAG = "BAG";
    public static String LENGTH = "LENGTH";
    private static String NAME = "NAME";

    private static String GOODS_FILE_PATH = Environment.getExternalStorageDirectory() + "/Download/goods.txt";

    public static List<Good> getGoodsFromFile() {
        File fl = new File( GOODS_FILE_PATH );
        FileInputStream fin;
        try {
            fin = new FileInputStream(fl);
            String json = convertStreamToString(fin);
            Type listType = new TypeToken<List<Good>>() {}.getType();
            List<Good> goods = new Gson().fromJson(json, listType);
            fin.close();
            return goods;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static void saveBag(Activity ctx, List<Good> goods) {
        SharedPreferences sPref = ctx.getSharedPreferences(NAME, Activity.MODE_APPEND);
        SharedPreferences.Editor ed = sPref.edit();
        String json = new Gson().toJson(goods);
        ed.putString(BAG, json);
        ed.commit();
    }

    public static List<Good> getBag(Activity act) {
        SharedPreferences sPref = act.getSharedPreferences(NAME, Activity.MODE_APPEND);
        String json = sPref.getString(BAG, "");
        Type listType = new TypeToken<List<Good>>() {}.getType();
        List<Good> goods = new Gson().fromJson(json, listType);
        return goods;
    }

    public static File getBagFileLocation(Activity act) {
        try {
            GsonBuilder builder = new GsonBuilder ();
            builder.registerTypeAdapter(Good.class, new CustomSerializer());
            Gson gson = builder.create();
            String json = gson.toJson(getBag(act));
            File testDirectory = new File(
                    Environment.getExternalStorageDirectory() + "/Download");
            if (!testDirectory.exists()) {
                testDirectory.mkdir();
            }
            File file = new File( testDirectory + "/order.txt");
            FileOutputStream fos = new FileOutputStream( file );
            OutputStreamWriter osw = new OutputStreamWriter(fos);
            osw.write(json);
            osw.flush();
            osw.close();
            fos.flush();
            fos.close();
            return file;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public static void saveFileLength(Activity act, int length){
        SharedPreferences sPref = act.getSharedPreferences(NAME, Activity.MODE_APPEND);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putInt(LENGTH, length);
        ed.commit();
    }

    public static int getFileLength(Activity act){
        SharedPreferences sPref = act.getSharedPreferences(NAME, Activity.MODE_APPEND);
        return sPref.getInt(LENGTH, 0);
    }

}
